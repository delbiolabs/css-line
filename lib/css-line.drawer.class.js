'use strict';

function Line()
{
    var self = this;
    self.create = create;
    self.rotate = rotate;
    self.getAngle = getAngle;
    self.getLength = getLength;

    /**
     * @param {Vector2D} a - {x: number, y: number}
     * @param {Vector2D} b - {x: number, y: number}
     * @param {Array} classes - css class to add into line element
     * @return {Object} line - jquery object
     */
    function create(a, b, classes)
    {
        var length = getLength(a, b);
        var angle  = getAngle(a, b);
        
		var line = $('<div>')
			.addClass('line '+classes.join(' '))
			.css({
			  'position': 'absolute',
			});
        rotate(line, angle);

        line.width(length)
			.offset({left: a.x, top: a.y});

        return line;
    }
    function rotate($line, angle)
    {
        var transform = 'rotate('+angle+'deg)';
        $line.css({
		  '-webkit-transform':  transform,
		  '-moz-transform':     transform,
		  'transform':          transform
		});
    }
    /**
     * @param {Vector2D} a - {x: number, y: number}
     * @param {Vector2D} b - {x: number, y: number}
     * @return {number} angle degree
     */
    function getAngle(a, b)
    {
        return Math.atan2(b.y - a.y, b.x - a.x) * 180 / Math.PI;
    }
    /**
     * @param {Vector2D} a - {x: number, y: number}
     * @param {Vector2D} b - {x: number, y: number}
     * @return {number} length
     */
    function getLength(a, b)
    {
        return Math.sqrt((a.x-b.x)*(a.x-b.x) + (a.y-b.y)*(a.y-b.y));
    }
}